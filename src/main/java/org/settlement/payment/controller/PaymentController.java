package org.settlement.payment.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.settlement.payment.util.GenerateIdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prowidesoftware.swift.model.mx.AbstractMX;
import com.prowidesoftware.swift.model.mx.MxPacs00800110;
import com.prowidesoftware.swift.model.mx.MxPain00100111;
import com.prowidesoftware.swift.model.mx.MxWriteConfiguration;
import com.prowidesoftware.swift.model.mx.dic.AccountIdentification4Choice;
import com.prowidesoftware.swift.model.mx.dic.ActiveOrHistoricCurrencyAndAmount;
import com.prowidesoftware.swift.model.mx.dic.AmountType4Choice;
import com.prowidesoftware.swift.model.mx.dic.BranchAndFinancialInstitutionIdentification6;
import com.prowidesoftware.swift.model.mx.dic.CashAccount40;
import com.prowidesoftware.swift.model.mx.dic.CreditTransferTransaction50;
import com.prowidesoftware.swift.model.mx.dic.CreditTransferTransaction54;
import com.prowidesoftware.swift.model.mx.dic.CustomerCreditTransferInitiationV11;
import com.prowidesoftware.swift.model.mx.dic.FIToFICustomerCreditTransferV10;
import com.prowidesoftware.swift.model.mx.dic.FinancialInstitutionIdentification18;
import com.prowidesoftware.swift.model.mx.dic.GenericAccountIdentification1;
import com.prowidesoftware.swift.model.mx.dic.GroupHeader95;
import com.prowidesoftware.swift.model.mx.dic.GroupHeader96;
import com.prowidesoftware.swift.model.mx.dic.PartyIdentification135;
import com.prowidesoftware.swift.model.mx.dic.PaymentIdentification13;
import com.prowidesoftware.swift.model.mx.dic.PaymentInstruction40;
import com.prowidesoftware.swift.model.mx.dic.PaymentMethod3Code;
import com.prowidesoftware.swift.model.mx.dic.PostalAddress24;

@RequestMapping("/payment")
@RestController
public class PaymentController {

  @Autowired
  private GenerateIdUtil generateIdUtil;

  @PostMapping(path = "/init", consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
  public ResponseEntity<?> initiation(@RequestBody String request) {
    // Baca request pain001 dari postman
    MxPain00100111 mxPain001 = new MxPain00100111();
    AbstractMX mx = AbstractMX.parse(request);

    // validasi mx id
    if (!"pain.001.001.11".equals(mx.getMxId().id())) {
      System.out.println("Request tidak bisa diparsing");
      return ResponseEntity.badRequest().body("Cannot parsing the request, please check the message id");
    }

    // parsing mx
    mxPain001 = (MxPain00100111) mx;
    // Dapatkan customer credit transfer initiationnya
    CustomerCreditTransferInitiationV11 pain001 = mxPain001.getCstmrCdtTrfInitn();
    // Dapatkan group headernya
    GroupHeader95 pain001Header = pain001.getGrpHdr();
    // Dapatkan payment informationsnya
    List<PaymentInstruction40> listPmtInf = pain001.getPmtInf();

    /*
     * LANJUTKAN CODE UNTUK MEMBUAT REQUEST PACS008 DARI INITIATION PAIN001
     */
    // Akan membuat request baru yaitu pacs008 untuk diteruskan ke settlementnya
    // Buat document pacs008nya
    MxPacs00800110 mxPacs008 = new MxPacs00800110();
    // Buat root element Financial institution to FI customer credit transfer
    FIToFICustomerCreditTransferV10 pacs008 = new FIToFICustomerCreditTransferV10();
    // Buat group header pacs008
    GroupHeader96 pacs008Header = new GroupHeader96();
    // buat credit transfer transaction information
    CreditTransferTransaction50 creditTransferTransaction = new CreditTransferTransaction50();

    /*
     * TULIS CODE DI SINI!
     */
    // set isi header
    pacs008Header.setMsgId(GenerateIdUtil.messageId("BBB"));
    pacs008Header.setCreDtTm(OffsetDateTime.now());
    pacs008Header.setNbOfTxs(pain001Header.getNbOfTxs());

    // cek dbtr agt
    listPmtInf.forEach(pmtInf -> {
      pacs008Header.setInstgAgt(pmtInf.getDbtrAgt());

      pmtInf.getCdtTrfTxInf().forEach(cdtTrfTxInf -> {
        pacs008Header.setInstdAgt(cdtTrfTxInf.getCdtrAgt());

        // set isi cdtTrfTxInf
        PaymentIdentification13 pmtIdPacs008 = new PaymentIdentification13();
        pmtIdPacs008.setEndToEndId(cdtTrfTxInf.getPmtId().getEndToEndId());
        pmtIdPacs008.setInstrId(cdtTrfTxInf.getPmtId().getInstrId());
        pmtIdPacs008.setTxId(GenerateIdUtil.instrIdOrTxId("TRF", "IDR"));

        creditTransferTransaction.setPmtId(pmtIdPacs008);
      });
    });

    // set header dan bodynya ke pacs008
    pacs008.setGrpHdr(pacs008Header);
    pacs008.getCdtTrfTxInf().add(creditTransferTransaction);

    // set pacs008 ke document
    mxPacs008.setFIToFICstmrCdtTrf(pacs008);
    System.out.println(mxPacs008.message());

    // conffiguration document iso20022
    MxWriteConfiguration configuration = new MxWriteConfiguration();
    configuration.documentPrefix = null;

    // Hit other api dari pacs008 ke settlement systemnya
    String result = null;
    HttpPost httpPost = new HttpPost("https://44db-114-10-116-97.ngrok-free.app/transfer");
    StringEntity entity = new StringEntity(mxPacs008.message(configuration), ContentType.APPLICATION_XML);
    httpPost.setEntity(entity);

    try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
      try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
        System.out.println(response.getVersion()); // HTTP/1.1
        System.out.println(response.getCode()); // 200
        System.out.println(response.getReasonPhrase()); // OK

        HttpEntity entity2 = response.getEntity();
        result = EntityUtils.toString(entity2);
        // Ensure that the stream is fully consumed
        EntityUtils.consume(entity2);
      }
    } catch (IOException | ParseException e) {
      e.printStackTrace();
      return ResponseEntity.internalServerError().body(e.getMessage());
    }

    return ResponseEntity.ok().body(result);
  }

  @GetMapping(path = "/getpain001", produces = MediaType.APPLICATION_XML_VALUE)
  public ResponseEntity<?> getPain001() {
    MxPain00100111 mxPain = new MxPain00100111();
    CustomerCreditTransferInitiationV11 pain001 = new CustomerCreditTransferInitiationV11();

    // group header
    GroupHeader95 header = new GroupHeader95();
    header.setMsgId(GenerateIdUtil.messageId("AAA"));
    header.setCreDtTm(OffsetDateTime.now());
    header.setNbOfTxs("1");

    // party identification 135
    PartyIdentification135 pty = new PartyIdentification135();
    pty.setNm("Femil");

    // postal address 24
    PostalAddress24 postalAddress = new PostalAddress24();
    postalAddress.setCtry("Indonesia");
    postalAddress.setStrtNm("Jatinegara");
    pty.setPstlAdr(postalAddress);

    header.setInitgPty(pty);

    pain001.setGrpHdr(header);

    // body payment informations
    PaymentInstruction40 pmtInf = new PaymentInstruction40();
    pmtInf.setPmtInfId("ABC/001");
    pmtInf.setPmtMtd(PaymentMethod3Code.TRF);
    pmtInf.setDbtr(pty);

    // debitor acc
    CashAccount40 dbtrAcct = new CashAccount40();
    dbtrAcct.setCcy("IDR");

    // no.rek
    AccountIdentification4Choice acctId = new AccountIdentification4Choice();
    acctId.setOthr(new GenericAccountIdentification1().setId("09090807"));

    dbtrAcct.setId(acctId);
    pmtInf.setDbtrAcct(dbtrAcct);

    BranchAndFinancialInstitutionIdentification6 dbtrAgt = new BranchAndFinancialInstitutionIdentification6();
    dbtrAgt.setFinInstnId(new FinancialInstitutionIdentification18().setBICFI("BNK1"));
    pmtInf.setDbtrAgt(dbtrAgt);

    CreditTransferTransaction54 cdtTrfTx = new CreditTransferTransaction54();
    AmountType4Choice amt = new AmountType4Choice();
    ActiveOrHistoricCurrencyAndAmount instdAmt = new ActiveOrHistoricCurrencyAndAmount();

    instdAmt.setCcy("IDR");
    instdAmt.setValue(BigDecimal.valueOf(1000000));
    amt.setInstdAmt(instdAmt);
    cdtTrfTx.setAmt(amt);

    PartyIdentification135 cdtr = new PartyIdentification135();
    cdtr.setNm("Ajeng");

    PostalAddress24 pstlAddrCdr = new PostalAddress24();
    pstlAddrCdr.setCtry("Indonesia");
    pstlAddrCdr.setStrtNm("Kampung Melayu");

    cdtr.setPstlAdr(pstlAddrCdr);
    cdtTrfTx.setCdtr(cdtr);

    CashAccount40 cdtrAcct = new CashAccount40();
    acctId.setOthr(new GenericAccountIdentification1().setId("9999999"));
    cdtrAcct.setId(acctId);
    cdtTrfTx.setCdtrAcct(cdtrAcct);

    BranchAndFinancialInstitutionIdentification6 cdtrAgt = new BranchAndFinancialInstitutionIdentification6();
    cdtrAgt.setFinInstnId(new FinancialInstitutionIdentification18().setBICFI("BNKA"));
    cdtTrfTx.setCdtrAgt(cdtrAgt);

    pmtInf.getCdtTrfTxInf().add(cdtTrfTx);
    pain001.addPmtInf(pmtInf);

    mxPain.setCstmrCdtTrfInitn(pain001);

    // write message
    MxWriteConfiguration configuration = new MxWriteConfiguration();
    configuration.documentPrefix = null;
    String xml = mxPain.message(configuration);

    return ResponseEntity.ok().body(xml);
  }
}
