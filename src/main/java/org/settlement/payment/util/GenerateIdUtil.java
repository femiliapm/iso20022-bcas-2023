package org.settlement.payment.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

@Component
public class GenerateIdUtil {

  public static String messageId(String prefix) {
    LocalDate date = LocalDate.now();
    String formattedDate = date.format(DateTimeFormatter.ofPattern("yyMMdd"));
    String suffix = "CCT" + String.valueOf((int) Math.random());
    return prefix + "/" + formattedDate + "/" + suffix;
  }

  public static String instrIdOrTxId(String prefix, String currency) {
    LocalDate date = LocalDate.now();
    String formattedDate = date.format(DateTimeFormatter.ofPattern("yyMMdd"));
    String suffix = String.valueOf((int) Math.random());
    return prefix + "/" + formattedDate + "-CCT/" + currency + "/" + suffix;
  }

  public static String endToEndId(String prefix) {
    String randInt = String.valueOf((int) Math.random());
    LocalDate date = LocalDate.now();
    String formattedDate = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    return prefix + "/" + randInt + "/" + formattedDate;
  }

}
