<h1 align="center">
  ISO20022 with Spring Boot
</h1>

<p align="center"><img src="https://yt3.ggpht.com/ytc/AKedOLT7YD9x6PiR-CfbBbFC3wz2WatiIZFrI_I0v-6k=s900-c-k-c0x00ffffff-no-rj" width="400px" alt="Arkademylogo.svg" /></p>

<p align="center">
    <a href="https://www.fazztrack.com/" target="blank">Our Website</a>
    ·
    <a href="https://www.fazztrack.com/class/fullstack-website-dan-golang">Join With Us</a>
    ·
</p>

## 🛠️ Installation Steps

1. Clone the repository

```bash
git clone https://gitlab.com/femiliapm/iso20022-bcas-2023.git
```

2. Install dependencies

```bash
mvnw clean install
# or
mvn clean install
```

3. Run the app

```bash
mvnw spring-boot:run
# or
mvn spring-boot:run
```

🌟 You are all set!

## 📘 Dictionary
#
### Bank Code

| Code | Name       |
| ---- | ---------- |
| BNK1 | Bank One   |
| BNK2 | Bank Two   |
| BNK3 | Bank Three |
| BNKA | Bank A     |
| BNKB | Bank B     |
| BNKC | Bank C     |

#
### Currency Code

| Code | Name                 |
| ---- | -------------------- |
| IDR  | Indonesia Rupiah     |
| USD  | United States Dollar |

#
### Status Code

| Code | Description       |
| ---- | ----------------- |
| ACCP | Transfer accepted |
| RJCT | Transfer rejected |

#
### Error Reason Code

| Code | Description                       |
| ---- | --------------------------------- |
| ERR1 | Source bank is not available      |
| ERR2 | Destination bank is not available |
| ERR3 | Invalid Amount                    |
| ERR4 | Currency is not allowed           |

#
## 💻 Built with

-   [Spring Boot](https://start.spring.io/): Java Framework

## 📖 More Documentation

-   [Spring Boot](https://spring.io/): Java Framework
-   [ISO20022](https://www.iso20022.org/iso-20022-message-definitions?page=0): Message Definition for ISO20022

<hr>
<p align="center">
Developed with ❤️ in Indonesia 	🇮🇩
</p>